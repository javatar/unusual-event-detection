import cv2
import numpy as np
import sys
from os import listdir
from os.path import isfile, join, isdir


class UCSD:
    def __init__(self, path, n, detect_interval):
        self.path = path
        self.fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()
        # self.fgbg = cv2.createBackgroundSubtractorMOG2()
        self.n = n
        self.detect_interval = detect_interval
        self.features = []
        self.labels = []

    def process_frame(self, bins, magnitude, frame, out, tag_image, fmask):
        bin_count = np.zeros(9, np.uint8)
        h,w, t = bins.shape
        # print('h,w,t', h,w,t)
        features_j = []
        labels_j = []
        # np.set_printoptions(threshold=sys.maxsize)
        # print(fmask.shape)

        if np.count_nonzero(fmask) > 0:
            for i in range(0, h, self.n):
                # print('HN',h, self.n)
                # verificamos si existe movimiento
                if np.count_nonzero(fmask[i]) > 0:
                    for j in range(0, w, self.n):
                        # print('WN',0, w, self.n)
                        i_end = min(h, i+self.n)
                        j_end = min(w, j+self.n)
                        # print('IJ', i_end, j_end)
                        if np.count_nonzero(fmask[i:i_end, j:j_end]):

                            # Get the atom for bins
                            atom_bins = bins[i:i_end, j:j_end].flatten()

                            # Average magnitude
                            atom_mag = magnitude[i:i_end, j:j_end].flatten().mean()
                            atom_fmask = frame[i:i_end, j:j_end].flatten()
                            # print('#######')
                            # print(atom_bins.shape)
                            # print('#######')

                            # Count of foreground values
                            f_cnt = np.count_nonzero(atom_fmask)
                            f_cnt_2 = np.count_nonzero(fmask[i:i_end, j:j_end].flatten())

                            # print('FCNT',f_cnt, f_cnt_2)

                            # Get the direction bins values
                            hs, _ = np.histogram(atom_bins, np.arange(10))
                            # print('HISTOGRAM', hs.shape)

                            # get the tag atom
                            tag_atom = tag_image[i:i_end, j:j_end].flatten()
                            # print('TAG_ATOM', tag_atom)
                            ones = np.count_nonzero(tag_atom)
                            zeroes = len(tag_atom) - ones
                            tag = 1
                            if ones < 50:
                                tag = 0
                            features = hs.tolist()
                            # print('FEAT', len(features))

                            features.extend([f_cnt, f_cnt_2, atom_mag, i, i+self.n, j, j+self.n, tag])
                            features_j.append(features[:-1])
                            labels_j.append(tag)
                            # print('feat',features)
                            for f in features:
                                out.write(str(f) + " ")
                            out.write("\n")
        return features_j, labels_j

    def extract_features(self, video_name, type, tag_video = ""):
        mag_threshold=1e-3
        elements = 0
        is_tagged = not tag_video == ""
        out = open("features/features_train_"+type+"_"+video_name.split("/")[0]+".txt","w")
        files = [f for f in listdir(self.path+video_name) if isfile(join(self.path+video_name, f))]
        if is_tagged:
            files_tag = [f for f in listdir(self.path+tag_video) if isfile(join(self.path+tag_video, f))]
            if '.DS_Store' in files_tag:
                files_tag.remove('.DS_Store')
            if '._.DS_Store' in files_tag:
                files_tag.remove('._.DS_Store')
            files_tag.sort()
        if '.DS_Store' in files:
            files.remove('.DS_Store')
        if '._.DS_Store' in files:
            files.remove('._.DS_Store')
        files.sort()
        number_frame = 0
        old_frame = None
        mots = []
        old_frame = cv2.imread(self.path + video_name + '001.tif', cv2.COLOR_BGR2GRAY)
        width = old_frame.shape[0]
        height = old_frame.shape[1]
        h, w = old_frame.shape[:2]
        bins = np.zeros((h, w, self.detect_interval), np.uint8)
        mag = np.zeros((h, w, self.detect_interval), np.float32)
        fmask = np.zeros((h, w, self.detect_interval), np.uint8)
        fmask_copy = np.zeros((h, w, self.detect_interval), np.uint8)
        frames = np.zeros((h, w, self.detect_interval), np.uint8)
        tag_img = np.zeros((h,w,self.n), np.uint8)

        hsv_mask = np.zeros((h, w, 3), np.uint8)
        # print(hsv_mask.shape)
        hsv_mask[:,:,1] = 255

        if is_tagged:
            tag_img = np.zeros((h,w,self.n), np.uint8)
        for tif in files:
            movement = 0
            frame = cv2.imread(self.path + video_name + tif, cv2.IMREAD_GRAYSCALE)
            
            frameCopy = frame.copy()
            
            frame = cv2.GaussianBlur(frame, (5,5), 10)
            fmask[...,number_frame % self.detect_interval] = self.fgbg.apply(frame)
            fmask_copy[...,number_frame % self.detect_interval] = self.fgbg.apply(frameCopy)
            
            flow = cv2.calcOpticalFlowPyrLK(old_frame, frame, None, 0.5, 3, 15, 3, 5, 1.2, 0)

            print(tif, flow)

            if is_tagged:
                tag_img_ = cv2.imread(self.path + tag_video + files_tag[number_frame] ,cv2.IMREAD_GRAYSCALE)
                tag_img[...,number_frame % self.detect_interval] = tag_img_
            
            height, width = flow.shape[:2]
            fx, fy = flow[:,:,0], flow[:,:,1]
            angle = ((np.arctan2(fy, fx+1) + 2*np.pi)*180)% 360
            binno = np.ceil(angle/45)
            magnitude = np.sqrt(fx*fx+fy*fy)

            hsv_mask[:,:,0] = binno
            hsv_mask[:,:,2] = cv2.normalize(magnitude, None, 0, 255, cv2.NORM_MINMAX)

            bgr = cv2.cvtColor(hsv_mask, cv2.COLOR_HSV2BGR)

            

            # x_mag, x_ang = cv2.cartToPolar(flow[:,:,0], flow[:,:,1], angleInDegrees=True)
            # print('METODO mag', x_mag)
            # print('METODO ang', x_ang)

            binno[magnitude < mag_threshold] = 0
            bins[...,number_frame % self.detect_interval] = binno
            mag[..., number_frame % self.detect_interval] = magnitude

            if number_frame % self.detect_interval == 0:
                feat, label = self.process_frame(bins, mag, frameCopy, out, tag_img, fmask)
                # print('fl', len(feat), len(label))
                self.features.extend(feat)
                self.labels.extend(label)

            # cv2.imshow('PUCP - Flujo Optico Denso', bgr)
            cv2.imshow('PUCP - Imagen Original', frameCopy)
            #cv2.imshow('Foreground extraction - with Gaussian Blur', fmask)
            #cv2.imshow('Foreground extraction - without Gaussian Blur', fmask_copy)
            # cv2.imshow('PUCP - Gaussian Blur', img_blur)

            number_frame += 1
            old_frame = frame
            k = cv2.waitKey(30) & 0xff
            if k == 27:
                break
        cv2.destroyAllWindows()
        out.close()

if __name__ == '__main__':
    ucsdped = 'UCSDped1'
    ucsd_training = UCSD('data/UCSD_Anomaly_Dataset.v1p2/'+ucsdped+'/Train/', 10, 1)
    dir_trains = [f for f in listdir('data/UCSD_Anomaly_Dataset.v1p2/'+ucsdped+'/Train/') if isdir(join('data/UCSD_Anomaly_Dataset.v1p2/'+ucsdped+'/Train/', f))]
    dir_tests = [f for f in listdir('data/UCSD_Anomaly_Dataset.v1p2/'+ucsdped+'/Test/') if isdir(join('data/UCSD_Anomaly_Dataset.v1p2/'+ucsdped+'/Test/', f))]
    dir_trains.sort()
    # out = open("features_test_UCSDped1.txt","w")

    for directory in dir_trains:
        ucsd_training.extract_features(directory+'/', ucsdped)

    # dir_trains.pop(0)

    # print(dir_trains, dir_tests)
    # print(len(dir_trains), len(dir_tests))

    # li = ["Test/Test003","Test/Test004","Test/Test014","Test/Test018","Test/Test019", "Test/Test021","Test/Test022","Test/Test023","Test/Test024","Test/Test032"]
    # ucsd_training.path = 'data/UCSD_Anomaly_Dataset.v1p2/'+ucsdped+'/'
    # for directory in li:
    #     print(directory)
    #     if not directory.endswith("gt"):
    #         print("PASO")
    #         dir_split = directory.split("/")[1]
    #         dir_split = dir_split + '_gt'
    #         if dir_split in dir_tests:
    #             ucsd_training.extract_features(directory+'/', ucsdped, directory + '_gt/')
    #         else:
    #             ucsd_training.extract_features(directory+'/', ucsdped)
