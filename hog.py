import matplotlib.pyplot as plt

from skimage.feature import hog
from skimage import data, exposure
import cv2

image = cv2.imread('data/UCSD_Anomaly_Dataset.v1p2/UCSDped1/Train/Train010/001.tif')

fd, hog_image = hog(image, orientations=10, pixels_per_cell=(5, 5),
                    cells_per_block=(1, 1), visualize=True, multichannel=True)

print(fd, hog_image)

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)

ax1.axis('off')
ax1.imshow(image, cmap=plt.cm.gray)
ax1.set_title('PUCP - Imagen UCSDpeds1')

# Rescale histogram for better display
hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 10))

ax2.axis('off')
ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
ax2.set_title('PUCP - HOG')
plt.show()